# -*- coding: UTF-8 -*-

import os
import sys
sys.path.append('lib')

from flask import Flask
from flask import request
from flask import send_file
from flask import abort

from random import randrange
from collections import OrderedDict
from time import time

from CairoPlot import dot_line_plot as graphic_dot_line
from CairoPlot import bar_plot as graphic_bar
from CairoPlot import pie_plot as graphic_pie
from CairoPlot import donut_plot as graphic_donut

from os.path import exists

CHARTS = os.path.join(os.path.dirname(__file__), "charts")

if (not os.path.exists(CHARTS)):
    os.mkdir(CHARTS)

cache = {}

def replace_acents(request):
    request = eval(request)
    acentos = {"a": "á", "e": "é", "i": "í", "o": "ó", "u": "ú", "n": "ñ"}

    for key in request:
        nkey = ""

        x = 0

        for letter in key:
            if x >= len(key): break

            if key[x] == "[":
                letra = key[x +1 : x + 2]

                nkey += acentos[letra]

                x += 2
                continue
            elif len(key) - 1 >= x:
                if key[x] != "]":
                    nkey += key[x]
                x += 1


        print nkey
        if (key != nkey and nkey):
            print 2
            request[nkey] = request[key]
            del request[key]

    return request

def add_to_cache(request, ubication):
    if len(cache) > 120:
        for request, ubication in cache.items():
            os.remove(ubication)
        cache = {}

    cache[request] = ubication


def get_graphic_request(tipo, request, width, height):
    graphic = Graphic()
    id = hash(request + tipo + str(width) + str(height))

    if id in cache and 2 == 3:
        if exists(cache[id]):
            return cache[id]
        else:
            cache.remove(id)

    jrequest = replace_acents(request) #.decode("utf-8-sig"))

    ubication = os.path.join(CHARTS, str(hash(int(time()) + randrange(8854))) + ".png")
    if tipo == "line":
        ubication = graphic.compare_data(width, height, ubication, jrequest)
    elif tipo == "pizza":
        ubication = graphic.pizza(width, height, ubication, jrequest)
    elif tipo == "bar":
        ubication = graphic.bar_plot(width, height, ubication, jrequest)
    elif tipo == "donut":
        ubication = graphic.donut(width, height, ubication, jrequest)
    elif tipo == "bar_negative":
        bar_plot_negative(width, height, ubication, jrequest)

    if ubication:
        cache[id] = ubication
        return ubication
    else:
        return

def _eval(codes):
    items = []
    for code in codes:
        items.append(eval(code))
    return items


class Graphic():
    def test_all(self):
        self.compare_data([[1, 2, 3, 4], [5, 1, 5, 3], [4, 2, 5, 1]], 
                           ["Test 1", "Test 2", "Test 3", "Test 4"])
        self.pizza({"john": 30, "mary": 20, "philip": 40 , "suzy": 20})
        self.bar_plot([20, 42, 4, 12, 5], ["Test 1", "Test 2", "Test 3", 
                                           "Test 4", "Test 5"])
        self.donut({"carl" : 123, "fawn" : 489, "susan" : 890 , "lavon" : 235})

    def bar_plot(self, width, height, ubication, data):
        values = []
        labels = []
        v_labels = []

        for label, value in data.items():
            labels.append(str(label))
            values.append(value)

        v_labels.append("0")

        all_values = []
        for label in values:
            all_values.append(label)

        for value in range(0, max(values)+1):
            if not value in all_values:
                if value == 0:
                    continue
                v_labels.append("")
            else:
                v_labels.append(str(value))
                
        graphic_bar(ubication, values, width, height, h_labels=labels, border=10,
                            v_labels=v_labels, grid=True, rounded_corners=True, colors=self.get_colours(len(values)))
        return ubication

    def bar_plot_negative(self, width, height, ubication, data):
        values = []
        labels = []
        v_labels = []
        colores = []

        for label, value in data.items():
            labels.append(str(label))
            values.append(value)

        v_labels.append("0")

        all_values = []
        for label in values:
            all_values.append(label)

        for value in range(0, max(values)+1):
            if not value in all_values:
                if value == 0:
                    continue
                v_labels.append("")
            else:
                v_labels.append(str(value))
        
        for valor in values:
            if valor < 0:
                colores.append((1,0,0))
            else:
                colores.append((0,1,0))

        graphic_bar(ubication, values, width, height, h_labels=labels, three_dimension=True, border=10,
                            v_labels=v_labels, grid=True, rounded_corners=True, colors=colores)
        return ubication

    def donut(self, width, height, ubication, values={}):
        graphic_donut(ubication, values, width, height, gradient=True,
                    shadow=True, background=None, colors=self.get_colours(len(values)))
        return ubication

    def pizza(self, width, height, ubication, values):
        # {"john": 20, "mary": 20, "philip": 40 , "suzy": 20} -Ejemplo
        new_value = {}

        count = 0
        unidad = ""
        for value in values:
            try:
                count += int(values[value])
            except:
                pass
        
        if count == 100:
            unidad = "%"

        for value in values:
            new_value['%s: (%s%s)' % (value, values[value], 
                      unidad)] = values[value]

        graphic_pie(ubication, new_value, width, height, shadow=True, gradient=True,
                                colors=self.get_colours(len(new_value)))
        return ubication

    def compare_data(self, width, height, ubication, values={}):
        if not values:
            return "Error!"

        if not v_labels:
            v_labels = []
            for value in values:
                for label in range(0, max(values.values())):
                    v_labels.append(str(label))

        graphic_dot_line(ubication, values,
                         width, height, None, 5, True, True,
                         dots=(50, 50, 50, 50), h_labels=values.keys())
        return ubication

    def get_colours(self, len_colours):
        colours = [(1, 0, 0), # Rojo
                   (0, 0.650980392157, 0.0235294117647), # Verde
                   (1, 0.58431372549, 0), # Anaranjado
                   (0, 0, 1), # Azul
                   (1, 0.991, 0), # Amarillo
                   (0, 0.61568627451, 1), # Turquesa
                   (1, 0, 0.764705882353)] # Rosado

        pallet = []

        if len_colours <= len(colours):
            for colour in colours[0:len_colours]:
                pallet.append(colour)
        else:
            index = 0
            for x in range(0, len_colours):
                if colours[index] != colours[-1]:
                    index += 1
                else:
                    index = 0
                pallet.append(colours[index])

        return pallet


#GatherData('pizza {"john": 20, "mary": 20, "philip": 40 , "suzy": 20}\n' +
#           'donut {"carl" : 123, "fawn" : 489, "susan" : 890 , "lavon" : 235}\n' +
#           'compare_data [[[1, 2, 3, 4], [5, 1, 5, 3], [4, 2, 5, 1]], ["Test 1", "Test 2", "Test 3", "Test 4"]]\n' +
#           'bar [20, 42, 4, 12, 5]|["Test 1", "Test 2", "Test 3", "Test 4", "Test 5"]')

app = Flask(__name__)
app.debug = True

@app.route('/')
def index():
    return "This is the chart server"

@app.route('/chart', methods=["GET"])
def chart():
    width = 700 if not "width" in request.args else int(request.args["width"])
    height = 500 if not "height" in request.args else int(request.args["height"])
    path = get_graphic_request(request.args["type"], 
                               request.args["data"], 
                               width, height)

    return send_file(path) if path else abort(500)

if (__name__ == "__main__"):
    app.run(host="0.0.0.0", port=5000)
